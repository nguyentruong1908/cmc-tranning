package com.cmcglobal.tranning.repository.specification;

import com.cmcglobal.tranning.domain.User;
import com.cmcglobal.tranning.repository.root.Permission_;
import com.cmcglobal.tranning.repository.root.User_;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

@UtilityClass
public class UserSpecification {

    public Specification<User> filterByPermissionCode(String permissionCode) {
        if (!StringUtils.hasText(permissionCode)) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }

        return hasPermissionCode(permissionCode);
    }

    public Specification<User> hasPermissionCode(String permissionCode) {
        return (root, cq, cb) -> cb.equal(root.join(User_.PERMISSIONS).get(Permission_.CODE), permissionCode);
    }
}
