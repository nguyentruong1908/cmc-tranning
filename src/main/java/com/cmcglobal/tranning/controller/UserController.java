package com.cmcglobal.tranning.controller;

import com.cmcglobal.tranning.dto.UserCreateRequestDTO;
import com.cmcglobal.tranning.dto.UserDTO;
import com.cmcglobal.tranning.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    public List<UserDTO> findAll(@RequestParam(value = "permission", required = false) String permissionQuery,
                                 @RequestHeader("x-permission-x") String permissionHeader) {
        return userService.findAll(permissionHeader, permissionQuery);
    }


    @PostMapping
    public void add(@RequestBody UserCreateRequestDTO userCreateRequestDTO) {
        userService.add(userCreateRequestDTO);
    }
}
