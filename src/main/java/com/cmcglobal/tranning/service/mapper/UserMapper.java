package com.cmcglobal.tranning.service.mapper;

import com.cmcglobal.tranning.domain.Permission;
import com.cmcglobal.tranning.domain.User;
import com.cmcglobal.tranning.dto.PermissionDTO;
import com.cmcglobal.tranning.dto.UserDTO;
import lombok.experimental.UtilityClass;

import java.util.stream.Collectors;

@UtilityClass
public class UserMapper {
    public UserDTO mapToUserDTO(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .name(user.getUsername())
                .permissions(user.getPermissions().stream().map(UserMapper::mapToPermissionDTO).collect(Collectors.toList()))
                .build();
    }

    public PermissionDTO mapToPermissionDTO(Permission permission) {
        return PermissionDTO.builder().id(permission.getId()).code(permission.getCode()).name(permission.getName()).build();
    }

    public Permission mapToPermission(PermissionDTO dto) {
        Permission permission = new Permission();
        permission.setId(dto.getId());
        permission.setCode(dto.getCode());
        permission.setName(dto.getName());
        return permission;
    }
}
