package com.cmcglobal.tranning.service;

import com.cmcglobal.tranning.dto.UserCreateRequestDTO;
import com.cmcglobal.tranning.dto.UserDTO;

import java.util.List;

public interface UserService {
    List<UserDTO> findAll(String permissionHeader, String permissionQuery);

    void add(UserCreateRequestDTO userCreateRequestDTO);
}
