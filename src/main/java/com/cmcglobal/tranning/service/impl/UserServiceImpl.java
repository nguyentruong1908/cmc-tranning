package com.cmcglobal.tranning.service.impl;

import com.cmcglobal.tranning.domain.User;
import com.cmcglobal.tranning.dto.UserCreateRequestDTO;
import com.cmcglobal.tranning.dto.UserDTO;
import com.cmcglobal.tranning.repository.UserRepository;
import com.cmcglobal.tranning.repository.specification.UserSpecification;
import com.cmcglobal.tranning.service.UserService;
import com.cmcglobal.tranning.service.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public List<UserDTO> findAll(String permissionHeader, String permissionQuery) {
        List<User> users = userRepository.findAll(
                UserSpecification
                        .hasPermissionCode(permissionHeader)
                        .and(UserSpecification.filterByPermissionCode(permissionQuery))
        );
        return users.stream().map(UserMapper::mapToUserDTO).collect(Collectors.toList());
    }

    @Override
    public void add(UserCreateRequestDTO userCreateRequestDTO) {
        User user = new User();
        user.setUsername(userCreateRequestDTO.getUsername());
        user.setPassword(userCreateRequestDTO.getPassword());
        user.setPermissions(userCreateRequestDTO.getPermissions().stream().map(UserMapper::mapToPermission).collect(Collectors.toList()));
        userRepository.save(user);
    }
}
