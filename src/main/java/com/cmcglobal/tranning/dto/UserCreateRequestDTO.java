package com.cmcglobal.tranning.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserCreateRequestDTO {
    private String username;
    private String password;
    private List<PermissionDTO> permissions;
}
