package com.cmcglobal.tranning.dto;

import com.cmcglobal.tranning.dto.PermissionDTO;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserDTO {
    private Long id;
    private String name;
    private List<PermissionDTO> permissions;
}
